<?php
include 'fonction.php';
session_start();
if(isset($_GET["identifiantNew"]))
{
    createID($_GET["identifiantNew"], $_GET["mdpNew"]);
}
else if(isset($_GET["identifiant"]))
{
   connexion($_GET["identifiant"], $_GET["mdp"]);
}
else if(isset($_GET["nomChat"]))
{
    if(isConnected())
    {
        ajoutChaton($_GET["nomChat"], $_GET["pouvoir"]);
    }
    else
    {
        $_SESSION["error"] = "vous ne pouvez pas créer de chatons si vous n'êtes pas connecté";
        creeChatonDir();
    }
}
else if(isset($_GET["nomDel"]))
{
    if(isConnected())
    {
        delChaton($_GET["nomDel"]);
    }
    else
    {
        $_SESSION["error"] = "vous ne pouvez pas supprimer de chatons si vous n'êtes pas connecté";
        delChatonDir();
    }
}
else if(isset($_GET["chat1"]) && isset($_GET["chat2"]))
{
    if(isConnected())
    {
        $gagnant = combatChaton($_GET["chat1"], $_GET["chat2"]);
        combatChatonDir();
    }
    else
    {
        $_SESSION["error"] = "vous devez être connecté pour pouvoir faire un combat de chatons";
        combatChatonDir();
    }
}
else
{
    indexDir();
}
?>