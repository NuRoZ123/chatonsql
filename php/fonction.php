<?php
function startSQL()
{
    return new PDO("mysql:host=;dbname=", "", "");
}

function createID($ident, $motsdp)
{
    $bd = startSQL();
    $requete = $bd->query("SELECT * FROM UserPlaque");
    $canGO = true;
    while ($donnees = $requete->fetch())
    {
        if($ident == $donnees["UserIdentifiant"])
        {
            $canGO = false;
            break;
        }
    }
    if($canGO)
    {
        $requete = $bd->prepare("INSERT INTO UserPlaque(idUser,UserIdentifiant,UserMDP) VALUE(?,?,?)");
        $requete -> execute(array(0,$ident,ashPass($motsdp)));
        $requete -> execute(array(0,$ident,ashPass($motsdp)));
        $_SESSION["okRegister"] = "vous vous êtes bien enregistré, veuillez vous reconnecter";
        connexionDir();
    }
    else
    {
        $_SESSION["error"] = "cet utilisateur existe déjà!";
        connexionDir();
    }
    $bd = Null;
}

function connexion($ident, $motsdp)
{
    $bd = startSQL();
    $allSQL = array();
    $allID = array();
    $requete = $bd->query("SELECT * FROM UserPlaque");
    while ($donnees = $requete->fetch())
    {
        $allSQL[$donnees["UserIdentifiant"]] = $donnees["UserMDP"];
        $allID[$donnees["UserIdentifiant"]] = $donnees["idUser"];
    }

    $ok = false;
    foreach($allSQL as $identifiant => $Pass)
    {
        if($identifiant == $ident)
        {
            if(password_verify($motsdp, $Pass))
            {
                $_SESSION["nameUser"] = $ident;
                $_SESSION["idUser"] = $allID[$ident];
                $_SESSION["listeChaton"] = getListe();
                $ok = true;
                indexDir();
            }
        }
    }
    if($ok == false)
    {
        $_SESSION["error"] = "identifiant ou mot de passe incorrect!";
        connexionDir();
    }
    $bd = Null;
}

function ashPass($mdp)
{
    return password_hash($mdp, PASSWORD_DEFAULT);
}

function isConnected()
{
    $res = false;
    if(isset($_SESSION["nameUser"]))
    {
        $res = true;
    }
    return $res;
}

function deconect()
{
    if(isConnected())
    {
        unset($_SESSION["nameUser"]);
        unset($_SESSION["idUser"]);
        unset($_SESSION["listeChaton"]);
        indexDir();
    }
}

function ifError()
{
    $res = "";
    if(isset($_SESSION["error"]))
    {
        $res = $_SESSION["error"];
        unset($_SESSION["error"]);
    }
    return $res;
}

function ifIsRegister()
{
    $res = "";
    if(isset($_SESSION["okRegister"]))
    {
        $res = $_SESSION["okRegister"];
        unset($_SESSION["okRegister"]);
    }
    return $res;
}

function getPouvoir()
{
    $bd = startSQL();
    $requete = $bd->query("SELECT * FROM pouvoirCat");

    while ($donnees = $requete->fetch())
    {
        $allPouv[$donnees["powerType"]] = $donnees["idPower"];
    }
    return $allPouv;
}

function ajoutChaton($nom, $pouv)
{
    $listeChaton = $_SESSION["listeChaton"];
    $sameName = false;
    foreach($listeChaton as $nomListe => $pouvListe)
    {
        if($nomListe == $nom)
        {
            $sameName = true;
        }
    }

    if($sameName)
    {
        $_SESSION["error"] = "vous ne pouvez pas cree un chaton qui existe deja";
        creeChatonDir();
    }
    else {
        $bd = startSQL();
        $requete = $bd->prepare("INSERT INTO userCat(id,nomCat,powerCat) VALUE(?,?,?)");
        $requete->execute(array($_SESSION["idUser"], $nom, getPouvoir()[$pouv]));
        $listeChaton = $_SESSION["listeChaton"];
        $listeChaton[$nom] = $pouv;
        $_SESSION["listeChaton"] = $listeChaton;
        listeChatDir();
    }
}

function getListe()
{
    $bd = startSQL();
    $requete = $bd->query("SELECT id, nomCat, powerType FROM userCat INNER JOIN pouvoirCat ON userCat.powerCat=pouvoirCat.idPower");
    $listeChat = array();
    while ($donnees = $requete->fetch())
    {
        if($donnees["id"] == $_SESSION["idUser"])
        {
            $listeChat[$donnees["nomCat"]] = $donnees["powerType"];
        }
    }
    return $listeChat;
}

function delChaton($nom)
{
    $bd = startSQL();
    $requete = $bd->prepare("DELETE FROM `userCat` WHERE nomCat = (?)");
    $requete->execute(array($nom));
    $listeChaton = $_SESSION["listeChaton"];
    unset($listeChaton[$nom]);
    $_SESSION["listeChaton"] = $listeChaton;
    listeChatDir();
}

function combatChaton($chat1, $chat2)
{
    $listeChat = $_SESSION["listeChaton"];
    $gagnant = "";
    if($chat1 == $chat2)
    {
        $_SESSION["error"] = "Vous ne pouvez pas faire combattre un chat contre lui même";
    }
    else if($listeChat[$chat1] == $listeChat[$chat2])
    {
        $gagnant = "Combat : $chat1 versus $chat2 : égalité";
    }
    else if($listeChat[$chat1] == "eau" && $listeChat[$chat2] == "feu" || $listeChat[$chat1] == "feu" && $listeChat[$chat2] == "terre" || $listeChat[$chat1] == "terre" && $listeChat[$chat2] == "eau")
    {
        $gagnant = "Combat : $chat1 versus $chat2 : $chat1 vainqueur !";
    }
    else
    {
        $gagnant = "Combat : $chat1 versus $chat2 : $chat2 vainqueur !";
    }
    $_SESSION["gagnant"] = $gagnant;

}

function indexDir()
{
    header('Location: ../page/index.php');
    exit();
}

function connexionDir()
{
    header('Location: ../page/connexion.php');
    exit();
}

function creeChatonDir()
{
    header('Location: ../page/creeChaton.php');
    exit();
}

function listeChatDir()
{
    header('Location: ../page/listeChat.php');
    exit();
}

function delChatonDir()
{
    header('Location: ../page/supprimerChaton.php');
    exit();
}

function combatChatonDir()
{
    header('Location: ../page/combatChaton.php');
    exit();
}
if (!debug_backtrace())
{
    indexDir();
}
?>
