<!DOCTYPE html>
<?php
include '../php/fonction.php';
session_start();
deconect();
?>

<html>
    <head>
        <title>Page de connexion</title>
        <meta charset="UTF-8">
        <meta name="author" content="NuRoZ">
        <link rel="stylesheet" href="../css/universal.css">
        <link rel="stylesheet" href="../css/connexion.css">
    </head>

    <body>
        <header>
            <nav>
                <ul>
                    <li><a href="index.php">Accueil</a></li>
                    <li><a href="listeChat.php">liste des chatons</a></li>
                    <li><a href="creeChaton.php">ajouter un chaton</a></li>
                    <li><a href="supprimerChaton.php">supprimer un chaton</a></li>
                    <li><a href="combatChaton.php">combat des chatons</a></li>
                </ul>
            </nav>
        </header>
        <main>
            <br/>
            <span class="error"><?php echo ifError(); ?></span>
            <span style="color: green; position: relative; left: 30%;".okRegister><?php echo ifIsRegister(); ?></span>
            <div class="connect">
                <span class="title">connexion</span><br/><br/>
                <form method="get" action="../php/get.php">
                    <span class="centrer">identifiant</span><br/>
                    <input type="text" name="identifiant" required><br/><br/>
                    <span class="centrermdp">mot de passe</span><br/>
                    <input type="password" name="mdp" required><br/><br/><br/>
                    <button type="submit" style="position: relative; left: 3.5vh">connectez-vous</button>
                </form>
            </div>
            <br/>
            <hr>
            <br/>
            <div class="register">
                <span class="title">enregistrement</span><br/><br/>
                <form method="get" action="../php/get.php">
                    <span class="centrer">identifiant</span><br/>
                    <input type="text" name="identifiantNew" class="reg" required><br/><br/>
                    <span class="centrermdp">mot de passe</span><br/>
                    <input type="password" name="mdpNew" class="reg" required><br/><br/><br/>
                    <button type="submit" class="reg" style="position: relative; left: 3.5vh">enregistrez-vous</button><br/>
                </form>
                <br/><br/>
            </div>
        </main>
    </body>
</html>