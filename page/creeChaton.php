<!DOCTYPE html>
<?php
include '../php/fonction.php';
session_start();
$listePouvoir = getPouvoir();
?>

<html lang="fr">
<head>
    <title>cration de chaton</title>
    <link rel="stylesheet" href="../css/universal.css">
    <link rel="stylesheet" href="../css/creeChaton.css">
    <meta charset="UTF-8">
    <meta name="author" content="NuRoZ">
</head>

<body>
<header>
    <nav>
        <ul>
            <li><a href="index.php">Accueil</a></li>
            <li><a href="listeChat.php">liste des chatons</a></li>
            <li><a href="creeChaton.php">ajouter un chaton</a></li>
            <li><a href="supprimerChaton.php">supprimer un chaton</a></li>
            <li><a href="combatChaton.php">combat des chatons</a></li>
        </ul>
    </nav>
    <div class="connectButton">
        <img src="../image/icone%20connexion.png" class="imageButton">
        <?php
        if(isConnected()) {
            echo '<a href="../page/connexion.php" class="button">déconnection</a>';
        }
        else
        {
            echo '<a href="../page/connexion.php" class="button">se connecter</a>';
        } ?>
    </div>
</header>

<main>
    <h1>qu'elle est votre nouveau chaton?</h1>
    <span class="error"><?php echo ifError(); ?></span>
    <br/>
    <form method="get" action="../php/get.php" class="formulaire">
        <span>nom du chaton: </span><br/>
        <input type="text" name="nomChat" maxlength="20" required><br/><br/>
        <span>pouvoir du chaton:</span><br/>
        <select name="pouvoir">
            <?php
                foreach($listePouvoir as $pouvoir => $idPower)
                {
                    echo '<option value="'.$pouvoir.'">'.$pouvoir.'</option>';
                }
            ?>
        </select><br/>
        <br/>
        <button type="submit">valider !</button>
    </form>
</main>

</body>
</html>
