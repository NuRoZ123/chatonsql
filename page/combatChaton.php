<!DOCTYPE html>
<?php
include '../php/fonction.php';
session_start();
if(isConnected())
{
    $listeChaton = $_SESSION["listeChaton"];
}

if(isset($_SESSION["gagnant"]))
{
    $gagnant = $_SESSION["gagnant"];
    unset($_SESSION["gagnant"]);
}
?>

<html lang="fr">
    <head>
        <title>combat de chatons</title>
        <link rel="stylesheet" href="../css/universal.css">
        <link rel="stylesheet" href="../css/combatChaton.css">
        <meta charset="UTF-8">
        <meta name="author" content="NuRoZ">
    </head>

    <body>
        <header>
            <nav>
                <ul>
                    <li><a href="index.php">Accueil</a></li>
                    <li><a href="listeChat.php">liste des chatons</a></li>
                    <li><a href="creeChaton.php">ajouter un chaton</a></li>
                    <li><a href="supprimerChaton.php">supprimer un chaton</a></li>
                    <li><a href="combatChaton.php">combat des chatons</a></li>
                </ul>
            </nav>
            <div class="connectButton">
                <img src="../image/icone%20connexion.png" class="imageButton">
                <?php
                if(isConnected()) {
                    echo '<a href="../page/connexion.php" class="button">déconnection</a>';
                }
                else
                {
                    echo '<a href="../page/connexion.php" class="button">se connecter</a>';
                } ?>
            </div>
        </header>

        <style>
        h1
        {
        text-align: center;
        }

        .formulaire
        {
        text-align: center;
        }
        </style>

        <main>
            <h1>Combat de chatons</h1>
            <?php
            if(isset($_SESSION["error"]))
            {
                echo '<h2 style="color: red; position: relative; left: 20%">'.ifError().'</h2>';
            }
            else if(isset($gagnant)) {
                echo '<h2 style="position: relative; left: 30%">'.$gagnant.'</h2>';
            }
            ?>


            <form action="../php/get.php" method="get" class="formulaire">
                <select name="chat1">
                    <?php
                    if(isConnected())
                    {
                        foreach ($listeChaton as $nom => $pouv) {
                            echo '<option value="' . $nom . '">' . $nom . '</option>';
                        }
                    }
                    else
                    {
                        echo '<option value="None">None</option>';
                    }
                    ?>
                </select>
                <span>VERSUS</span>
                <select name="chat2">
                    <?php
                    if(isConnected())
                    {
                        foreach ($listeChaton as $nom => $pouv) {
                            echo '<option value="' . $nom . '">' . $nom . '</option>';
                        }
                    }
                    else
                    {
                        echo '<option value="None">None</option>';
                    }
                    ?>
                </select>
                <br/><br/>
                <button type="submit">combattre !</button>
            </form>
        </main>
    </body>
</html>
