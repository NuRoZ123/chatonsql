<!DOCTYPE html>
<?php
include '../php/fonction.php';
session_start();

if(isset($_SESSION["error"]))
{
    $error = $_SESSION["error"];
    unset($_SESSION["error"]);
}
else
{
    $error = "";
}

if(isConnected())
{
    $liste = $_SESSION["listeChaton"];
}
?>

<html lang="fr">
    <head>
        <title>suppresion chaton</title>
        <link rel="stylesheet" href="../css/universal.css">
        <link rel="stylesheet" href="../css/supprimerChaton.css">
        <meta charset="UTF-8">
        <meta name="author" content="NuRoZ">
    </head>

    <body>
        <header>
            <nav>
                <ul>
                    <li><a href="index.php">Accueil</a></li>
                    <li><a href="listeChat.php">liste des chatons</a></li>
                    <li><a href="creeChaton.php">ajouter un chaton</a></li>
                    <li><a href="supprimerChaton.php">supprimer un chaton</a></li>
                    <li><a href="combatChaton.php">combat des chatons</a></li>
                </ul>
            </nav>
            <div class="connectButton">
                <img src="../image/icone%20connexion.png" class="imageButton">
                <?php
                if(isConnected()) {
                    echo '<a href="../page/connexion.php" class="button">déconnection</a>';
                }
                else
                {
                    echo '<a href="../page/connexion.php" class="button">se connecter</a>';
                } ?>
            </div>
        </header>
        <main>
            <h1>Mise en retraite des chatons</h1>
            <?php echo '<span class="error">'.$error.'</span>';?>
            <form method="get" action="../php/get.php" class="formulaire">
                <span>choisissez votre chaton</span><br/>
                <select name="nomDel">
                    <?php
                        if(isConnected())
                        {
                            foreach($liste as $nom => $pouv)
                            {
                                echo '<option value="'.$nom.'">'.$nom.'</option>';
                            }
                        }
                        else
                        {
                            echo '<option value="None">None</option>';
                        }
                    ?>
                </select><br/><br/>

                <button type="submit">mise en retraite!</button>
            </form>
        </main>
    </body>
</html>
