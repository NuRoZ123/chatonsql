<!DOCTYPE html>
<?php
include '../php/fonction.php';
session_start();
if(isConnected())
{
    $listeChaton = $_SESSION["listeChaton"];
}
else
{
    $listeChaton = "";
    $_SESSION["error"] = "vous ne pouvez pas consulter votre liste si vous n'êtes pas connecté";
}
?>

<html lang="fr">
    <head>
        <title>liste des chats</title>
        <link rel="stylesheet" href="../css/universal.css">
        <link rel="stylesheet" href="../css/listeChat.css">
        <meta charset="UTF-8">
        <meta name="author" content="NuRoZ">
    </head>

    <body>
        <header>
            <nav>
                <ul>
                    <li><a href="index.php">Accueil</a></li>
                    <li><a href="listeChat.php">liste des chatons</a></li>
                    <li><a href="creeChaton.php">ajouter un chaton</a></li>
                    <li><a href="supprimerChaton.php">supprimer un chaton</a></li>
                    <li><a href="combatChaton.php">combat des chatons</a></li>
                </ul>
            </nav>
            <div class="connectButton">
                <img src="../image/icone%20connexion.png" class="imageButton">
                <?php
                if(isConnected()) {
                    echo '<a href="../page/connexion.php" class="button">déconnection</a>';
                }
                else
                {
                    echo '<a href="../page/connexion.php" class="button">se connecter</a>';
                }; ?>
            </div>
        </header>

        <main>
            <h1>Liste des Chatons</h1>
            <section class="liste">
                <?php
                if(isConnected())
                {
                    foreach($listeChaton as $nom => $pouv)
                    {

                        echo '<span style="position: relative; left: 10%; font-size: 4vh; width: 5vw"> nom: '. $nom .' pouvoir: '. $pouv.'</span><br/>';
                    }
                }
                else
                {
                    echo '<span class="error">'.$_SESSION["error"].'</span>';
                    unset($_SESSION["error"]);
                }
                ?>
            </section>
        </main>

    </body>
</html>
