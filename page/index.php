<!DOCTYPE html>
<?php
include '../php/fonction.php';
session_start();
?>

<html lang="fr">
<head>
    <title>Page d'accueil</title>
    <link rel="stylesheet" href="../css/universal.css">
    <link rel="stylesheet" href="../css/index.css">
    <meta charset="UTF-8">
    <meta name="author" content="NuRoZ">
</head>

<body>
    <header>
        <nav>
            <ul>
                <li><a href="index.php">Accueil</a></li>
                <li><a href="listeChat.php">liste des chatons</a></li>
                <li><a href="creeChaton.php">ajouter un chaton</a></li>
                <li><a href="supprimerChaton.php">supprimer un chaton</a></li>
                <li><a href="combatChaton.php">combat des chatons</a></li>
            </ul>
        </nav>
        <div class="connectButton">
            <img src="../image/icone%20connexion.png" class="imageButton">
            <?php
            if(isConnected()) {
                echo '<a href="../page/connexion.php" class="button">déconnection</a>';
            }
            else
            {
                echo '<a href="../page/connexion.php" class="button">se connecter</a>';
            } ?>
        </div>
    </header>

    <main>
        <h1>Bienvenue
            <?php
                if(isConnected())
                {
                    echo $_SESSION["nameUser"];
                }
                ?>
            !</h1>
        <iframe class="video" width="560" height="315" src="https://www.youtube-nocookie.com/embed/HmZKgaHa3Fg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        <p>Bienvenue au jeu du combat de chatons très mignons.<br/> Les règles sont très simples, il faut <br/>
            vous connectez grâce au bouton en haut à droite de la page ou en cliquant <a href="connexion.php">ici</a>.<br/>
            Suite à cela, vous allez devoir créer des chatons trop mignons (mais badass). Chaque chaton<br/>
            se fera attribuer un pouvoir entre le feu, l'eau ou la terre. Tous ces chatons <br/>
            peuvent se battre entre eux car ils ont beau être mignons et sympas, ils ont très soif de sang,<br/>
            ceux-la peuvent se battre indéfiniment. Une fois que vous pensez que vos chatons se <br/>
            sont assez combattu, vous pouvez les envoyer en retraites.</p>

        <h2 class="titre">1: Où retrouver ses chatons ?</h2>
        <p>Pour retrouver ses chatons, il suffit de cliquer <a href="#">ici</a> ou de cliquer sur le bouton<br/>
            "liste des chatons" en haut de la page. Vos chatons s'afficheront sous forme de liste ou vous pourrez<br/>
            ainsi les modifier ou les supprimer.</p>

        <h2 class="titre">2: Où créer des chatons ?</h2>
        <p>Pour créer un chaton, il vous suffit de cliquer <a href="#">ici</a> ou de cliquer sur le bouton<br/>
            "ajouter un chaton" en haut de la page. Il vous sera demandé de saisir un nom et un pouvoir<br/>
            pour finir avec la création de votre chaton. Il suffira de cliquer sur le bouton "confirmer !"</p>

        <h2 class="titre">3: supprimer un chaton</h2>
        <p>Pour mettre en retraite un chaton, il vous suffit de cliquer <a href="#">ici</a> ou de cliquer<br/>
            sur le bouton "supprimer un chaton" en haut de la page. Pour le mettre en retraite, il faut<br/>
            juste sélectionner le chaton et cliquer sur le bouton "confirmer !" et voila votre chaton est <br/>
            parti vivre ses meilleurs années en temps que retraité!</p>

        <h2 class="titre">4: Faire combattre vos chatons</h2>
        <p>Vos chatons sont maintenant prêt au combat bien joué!!!! Pour créer un combat, veuillez cliquer<br/>
            sur le bouton en haut de la page "combat des chatons" ou cliquer <a href="#">ici</a>. Pour continuer<br/>
            il vous suffira seulement de selectionner deux chatons pour le combat! (il faut savoir que<br/>
            les chatons de type eau gagnent contre les chatons de type feu, ceux de type feu gagnent contre les<br/>
            chatons de type terre et pour finir les chatons de type terre gagnent contre ceux de type eau.)</p>
    </main>
</body>
</html>